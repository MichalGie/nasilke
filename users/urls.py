from django.conf.urls import url
from .views import ConfirmAnonymUser


urlpatterns = [
	url(r'^potwierdz-email/(?P<token>[\w-]+)/$', ConfirmAnonymUser.as_view(), name='confirm-email'),
]