from django.core.mail import EmailMultiAlternatives
from django.db import models
from django.template.loader import get_template
from django.template import Context
from django.shortcuts import reverse
from django.contrib import messages
from users.utils import random_token_generator
from nasilke.settings import site_full_url

# Create your models here.

class AnonymUser(models.Model):
	nick			= models.CharField(max_length=32, unique=True, blank=False, null=False)
	email 			= models.EmailField(unique=True, blank=False, null=False)
	newsletter		= models.BooleanField(default=False)
	huntletter		= models.BooleanField(default=False)
	confirmed		= models.BooleanField(default=False)
	banned			= models.BooleanField(default=False)
	token 			= models.CharField(max_length=30, blank=True, null=True)
	created			= models.DateTimeField(auto_now_add=True)

	@property
	def is_confirmed(self):
		return self.confirmed

	@property
	def is_banned(self):
		return self.banned


	def __str__(self):
		return self.email

	def send_activation_email(self):

		token_url = site_full_url + reverse('confirm-email', args=(self.token, ))
		plaintext = get_template('emails/activation.txt')
		htmly     = get_template('emails/activation.html')

		d = {
			'nick'	: self.nick,
			'email'	: self.email,
			'token_url'	: token_url,
		}

		subject, from_email, to = 'Aktywacja', 'admin@nasilke.com', self.email
		text_content = plaintext.render(d)
		html_content = htmly.render(d)
		msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
		msg.attach_alternative(html_content, "text/html")
		msg.send()


	def create_anonym_user(email, nick):
		user_email = AnonymUser.objects.filter(email=email)
		user_nick = AnonymUser.objects.filter(nick=nick)
		if (not user_email and not user_nick):
			author = AnonymUser.objects.create(email=email, nick=nick)

		else:
			user_nick = user_nick[0]
			user_email = user_email[0]
			if user_email.pk == user_nick.pk:
				author = user_email
			else:
				print('err')
				return author

		if not author.is_confirmed:
			author.nick = nick
			author.token = random_token_generator()
			author.save()
			author.send_activation_email()

		return author