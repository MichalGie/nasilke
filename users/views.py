from django.shortcuts import render, redirect
from django.views.generic.edit import UpdateView
# Create your views here.

from .models import AnonymUser


class ConfirmAnonymUser(UpdateView):

	def get(self, request, *args, **kwargs):
				
		token 	= kwargs.get('token')
		user 	= None

		if token:
			try:
				user= AnonymUser.objects.get(token=token)
			except:
				print('problemo msg')
				return redirect('/')

		if user:
			#  message active!!!
			user.confirmed = True
			user.token 	= ''
			user.save()

		return redirect('/')