import random
import string

def random_token_generator():
	size = random.randrange(25,31)
	chars=string.ascii_letters + string.digits
	return ''.join(random.choice(chars) for _ in range(size))