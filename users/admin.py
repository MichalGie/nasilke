from django.contrib import admin

# Register your models here.
from .models import AnonymUser

admin.site.register(AnonymUser)