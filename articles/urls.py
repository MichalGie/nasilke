from django.conf.urls import url

from .views import Home, ArticleListView, ArticleDetailView, ContactView, FirstStepsListView, ArticleCategoryListView

urlpatterns = [
	url(r'^artykuly/(?P<category_slug>[\w-]+)/$', ArticleCategoryListView.as_view(), name='articles-category'),

	url(r'^artykuly/$', ArticleListView.as_view(), name='articles'),
	url(r'^artykul-(?P<article_slug>[\w-]+)/$', ArticleDetailView.as_view(), name='article'),
	url(r'^kontakt/$', ContactView.as_view(), name='contact'),
	url(r'^zanim-zaczniesz/$', FirstStepsListView.as_view(), name='first-steps'),
	url(r'^$', Home.as_view(), name='home'),
]