from django import forms
from django.contrib import admin

# Register your models here.
from .models import Article, ArticleCategory
from .forms import ArticleAdminForm


class CustomArticleAdmin(admin.ModelAdmin):
	form = ArticleAdminForm
	fieldsets = (
	(None, {
		'fields': ('user', 'typee', 'category',  'title', 'tags', 'photo', 'photo_thumb', 'summary', 'content', 'sketch', 'publish_date', 'linked', )
		}),
		('Wygenerowane parametry', {
			'classes': ('collapse',),
			'fields': ('slug', ),
		}),
	)
	readonly_fields = ('photo_thumb', 'slug')
	list_display = ('title', 'sketch',  'category', 'tags', 'publish_date', 'updated', 'created')


admin.site.register(Article, CustomArticleAdmin)
admin.site.register(ArticleCategory)