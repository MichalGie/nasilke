from ckeditor_uploader.fields import RichTextUploadingField
from django.db import models
from django.db.models import Q
from django.db.models.signals import pre_save, post_save
from django.contrib.auth.models import User
from django.core.validators import MinLengthValidator
from django.dispatch import receiver
from django.utils.safestring import mark_safe
from django.utils.timezone import now
from django.forms import ValidationError
# Create your models here.

from nutritions.utils import unique_slug_generator

class ArticleCategory(models.Model):
	name 			= models.CharField(max_length=32, unique=True, verbose_name='Nazwa kategorii')
	slug 			= models.SlugField(blank=True, null=True)

	def __str__(self):
		return self.name

	class Meta:
		verbose_name = 'Kategoria Artykułów'
		verbose_name_plural = '2. Katerogrie Artykułów'

ARTICLE_STANDARD = 'Standardowy'
ARTICLE_BEFORE_START_MOTIVATION = 'Zanim zaczniesz -> Motywacja'
ARTICLE_BEFORE_START_BASICS = 'Zanim zaczniesz -> Podstawy'
ARTICLE_BEFORE_START_DIET = 'Zanim zaczniesz -> Dieta'
ARTICLE_BEFORE_START_ADVICES = 'Zanim zaczniesz -> Porady Byka'

ARTICLE_TYPE = (
	('SA', ARTICLE_STANDARD),
	('BM', ARTICLE_BEFORE_START_MOTIVATION),
	('BB', ARTICLE_BEFORE_START_BASICS),
	('BD', ARTICLE_BEFORE_START_DIET),
	('BA', ARTICLE_BEFORE_START_ADVICES),
	)

class CustomActiveArticlesManager(models.Manager):
	def get_queryset(self):
		return super(CustomActiveArticlesManager, self).get_queryset().filter(Q(sketch__exact=False) & Q(publish_date__date__lt=now())).order_by('-publish_date')

class Article(models.Model):
	user 			= models.ForeignKey(User, blank=False, null=False, verbose_name='Autor')
	typee			= models.CharField(choices=ARTICLE_TYPE, default=ARTICLE_STANDARD, max_length=2, blank=False, null=False, verbose_name='Typ')
	category 		= models.ForeignKey(ArticleCategory, blank=True, null=True, verbose_name='Kategoria')
	title			= models.CharField(max_length=128, blank=True, null=True, verbose_name='Tytuł')
	photo			= models.ImageField(upload_to='img/articles/', blank=True, null=True, verbose_name='Zdjęcie')
	summary			= models.CharField(max_length=255, null=True, blank=True, verbose_name='Zajawka')
	content 		= RichTextUploadingField(blank=True, null=True, verbose_name='Treść')
	tags			= models.CharField(max_length=255, blank=True, null=True, verbose_name='Tagi ( oddziel przecinkiem każdy )')
	linked			= models.ManyToManyField('self' , blank=True, verbose_name='Powiązane artykuły')
	sketch			= models.BooleanField(default=True, verbose_name='Szkic')
	publish_date  	= models.DateTimeField(default=now, verbose_name='Data opublikowania')


	slug			= models.SlugField(blank=True, verbose_name='Wygenerowany slug')
	updated			= models.DateTimeField(auto_now=True, verbose_name='Zmodyfikowany')
	created		 	= models.DateTimeField(auto_now_add=True, verbose_name='Utworzony')

	objects 		= models.Manager()
	active_articles	= CustomActiveArticlesManager()

	class Meta:
		verbose_name = 'Artykuł'
		verbose_name_plural = '1. Artykuły'

	def __str__(self):
		return self.title

	def photo_thumb(self):
		if self.photo:
			return mark_safe('<img src="{}" style="width:700px;"/>'.format(self.photo.url))
		else:
			return 'Brak wgranego zdjęcia'


@receiver(pre_save, sender=Article, dispatch_uid='article_pre_save_signal')
def article_pre_save_signal(sender, instance, **kwargs):
	if instance.title:
		instance.slug = unique_slug_generator(instance, instance.title)

@receiver(pre_save, sender=ArticleCategory, dispatch_uid='article_category_pre_save_signal')
def article_category_pre_save_signal(sender, instance, **kwargs):
	instance.name = instance.name.capitalize()
	if instance.name:
		instance.slug = unique_slug_generator(instance, instance.name)

# @receiver(pre_save, sender=ArticleTag, dispatch_uid='article_tag_pre_save_signal')
# def article_tag_pre_save_signa(sender, instance, **kwargs):
# 	instance.name = instance.name.capitalize()