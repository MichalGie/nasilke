from django import template

register = template.Library()

def addatr(value, arg):
	attrs = value.field.widget.attrs
	
	atr = arg.split(',')

	for string in atr:
		kv = string.split(':')
		attrs[kv[0]] = kv[1]

	rendered = str(value)

	return rendered

register.filter('addatr', addatr)