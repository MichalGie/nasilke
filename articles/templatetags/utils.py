from django.utils.timezone import now
from datetime import datetime, timedelta
from django import template
from django.utils.timesince import timesince
from django.template import Library
register = Library()

@register.filter
def stars(value):
	max_val = 5
	int_val = int(value)
	difference = value - int_val

	output = ''
	for c in range(int_val):
		output+='f'

	if difference:
		output+='h'

	for d in range(5-len(output)):
		output+='e'

	return output

@register.filter
def range(value):
	return 'x'*int(value)

@register.filter
def range_rest(value):
	return 'x'*(5-int(value))

@register.filter
def age(value):
	current_date = now()
	try:
		difference = current_date - value
	except:
		return value

	if difference <= timedelta(minutes=1):
		return 'just now'
	return '%(time)s ago' % {'time': timesince(value).split(', ')[0]}