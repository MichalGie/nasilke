from django.db.models import Q
from django.contrib import messages
from django.shortcuts import render
from django.utils.timezone import now
from django.views import View
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView

from .forms import ContactForm
from django.views.generic.edit import FormView

# Create your views here.
from .models import Article, ArticleCategory

class Home(View):
	def get(self, request, *args, **kwargs):
		articles = Article.active_articles.all()[:4]

		context = {
			'articles' : articles,
		}

		return render(request, 'index.html', context)

class ArticleListView(ListView):
	model = Article

	def get_queryset(self):
		return Article.active_articles.order_by('-publish_date')

	def get_context_data(self, **kwargs):
		context = super(ArticleListView, self).get_context_data(**kwargs)
		context['categories'] = ArticleCategory.objects.all()
		return context

class ArticleDetailView(DetailView):
	model = Article

	def get_object(self):
		article_slug = self.kwargs.get('article_slug')
		print(Article.objects.get(slug=article_slug))
		return Article.active_articles.get(slug=article_slug)


class ContactView(FormView):
	template_name = 'contact.html'
	form_class = ContactForm
	success_url = '/'

	def form_valid(self, form):
		form.send_email()
		messages.add_message(self.request, messages.INFO, 'Wysłano email na podany adres.')
		return super(ContactView, self).form_valid(form)


class FirstStepsListView(ListView):
	template_name = 'articles/first_steps_list.html'
	moodel = Article

	def get_queryset(self):
		return Article.active_articles.all()

	def get_context_data(self, **kwargs):
		context = super(FirstStepsListView, self).get_context_data(**kwargs)
		articles = Article.active_articles.all()
		context['articles_motivation'] = articles.filter(typee='BM')
		context['articles_basics'] = articles.filter(typee='BB')
		context['articles_diet'] = articles.filter(typee='BD')
		context['articles_advices'] = articles.filter(typee='BA')
		return context


class ArticleCategoryListView(ListView):
	model = Article
	template_name = 'articles/article_list.html'

	def get_queryset(self):
		article_category_slug = self.kwargs.get('category_slug')
		return Article.active_articles.filter(category__slug=article_category_slug)

	def get_context_data(self, **kwargs):
		context = super(ArticleCategoryListView, self).get_context_data(**kwargs)
		context['categories'] = ArticleCategory.objects.all()
		return context