from django import forms
from django.core.mail import send_mail
from django.core.validators import MinLengthValidator
from django.utils.timezone import now

from .models import Article


C1 = 'Problemy techniczne'
C2 = 'Niezgodnośc danych'
C3 = 'Pomysł usprawniający serwis'
C4 = 'Inny'
CONTACT_FORM_CHOICES = (
		('1', C1),
		('2', C2),
		('3', C3),
		('4', C4),
	)

class ContactForm(forms.Form):
	contact_type	= forms.ChoiceField(choices=CONTACT_FORM_CHOICES, required=True)
	name			= forms.CharField(max_length=70, validators=[MinLengthValidator(3)])
	email			= forms.EmailField(required=True)
	message			= forms.CharField(widget=forms.Textarea, validators=[MinLengthValidator(10)])

	def send_email(self):
		contact_type = self.cleaned_data['contact_type']
		message = self.cleaned_data['message']
		email = self.cleaned_data['email']
		send_mail(
			'{}'.format(contact_type),
			'{}'.format(message),
			'{}'.format(email),
			['mg@op.pl'],
			fail_silently=False,
		)

class ArticleAdminForm(forms.ModelForm):
	class Meta:
		model = Article
		fields = ()

	def clean(self):
		cleaned_data 	= super(ArticleAdminForm, self).clean()

		user			= cleaned_data.get('user')
		typee			= cleaned_data.get('typee')
		category		= cleaned_data.get('category')
		title			= cleaned_data.get('title')
		photo			= cleaned_data.get('photo')
		summary			= cleaned_data.get('summary')
		content			= cleaned_data.get('content')
		tags			= cleaned_data.get('tags')
		linked			= cleaned_data.get('linked')
		sketch			= cleaned_data.get('sketch')
		publish_date	= cleaned_data.get('publish_date')

		if not sketch:
			if not category:
				self.add_error('category', "Nie wybrano kategorii")
			if not typee:
				self.add_error('type_', "Nie wybrano typu")
			if not title:
				self.add_error('title', "Nie wybrano tytułu")
			if not photo:
				self.add_error('photo', "Nie wgrano zdjęcia")
			
			if not summary:
				self.add_error('summary', "Nie wprowadzono zajawki")
			elif len(summary) < 200:
				self.add_error('summary', "Wprowadzona zajawka jest krótsza niż 200 znaków")
			
			if not content:
				self.add_error('content', "Nie wprowadzono treści")
			
			if not tags:
				self.add_error('tags', "Nie podano tagów")
			elif len(tags) < 25:
				self.add_error('tags', "Wprowadzono za mało tagów (min 25 znaków)")

			# if publish_date < now():
			# 	self.add_error('publish_date', "Podana data publikacji jest w przeszłości")
			
			if not linked:
				self.add_error('linked', "Nie wybrano powiązwanych artykułów")
			elif linked.count() > 3:
				self.add_error('linked', "Wybrano więcej niż 3 powiązane artyukuły")
	

	def save(self, commit=True):
		instance = super(ArticleAdminForm, self).save(commit=False)

		if not instance.title:
			instance.title = 'Szkic - {}'.format(instance.user)

		if commit:
			instance.save()
		return instance	
