# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-10-19 15:41
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('nutritions', '0025_auto_20171019_1341'),
    ]

    operations = [
        migrations.AlterField(
            model_name='reviewfield',
            name='field',
            field=models.CharField(choices=[('RW1', 'Rozpuszczalność'), ('RW2', 'Smak'), ('RP1', 'Pompa'), ('RP2', 'Koncentracja'), ('RP3', 'Pobudzenie'), ('RP4', 'Czas działania'), ('RB1', 'Pobudzenie'), ('RB2', 'Komfort psychiczny'), ('RB3', 'Brak apetytu')], default='Rozpuszczalność', max_length=3),
        ),
    ]
