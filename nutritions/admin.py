from django.contrib import admin
from django import forms
from solo.admin import SingletonModelAdmin
from .models import Whey, Gainer, Bcaa, Creatine, Company, Nutrition, Flavour, PreWorkout, Burner,\
		Composition,NutritionComposition, CompositionField, NutritionForm, Configuration, ReviewField, NutritionReview, NutritionHunter
# Register your models here.



class CustomCompositionInline(admin.TabularInline):
    model = Composition
    extra = 5

    verbose_name = 'Składnik'
    verbose_name_plural = 'Składniki'




class CustomNutriotionAdmin(admin.ModelAdmin):
	fieldsets = (
	(None, {
		'fields': ( 'name', 'ceneo_id', 'company', 'photo_thumb', 'photo', 'price', 'weight', 'portion', 'flavour', 'form', \
			'kcal', 'description','expert_note', 'featured', 'available')
		}),
		('Wygenerowane parametry', {
			'classes': ('collapse',),
			'fields': ('rating_supositioner', 'rating', 'portion_amount', 'slug'),
		}),
	)
	readonly_fields = ('photo_thumb', 'rating', 'portion_amount', 'slug', 'rating_supositioner')
	list_display = ('name', 'company', 'featured', 'available')
	inlines = [CustomCompositionInline,]


	# def save_related(self, request, form, formsets, change):
	# 	print(form)
	# 	print('aaa')
	# 	# if not form.cleaned_data['type']:
	# 	# 	type, created = Type.objects.get_or_create(name="Teacher")
	# 	# 	form.cleaned_data['type'] = [type]
	# 	# 	form.save_m2m()
	# 	for formset in formsets:
	# 		print(formset)
	# 		self.save_formset(request, form, formset, change=change)



	def save_model(self, request, obj, form, change):
		for related in obj.composition.all():
			related.nutrition = obj
			related.nutrition_composition.field.save()
			related.nutrition_composition.save()
			related.save()

		obj.rating = obj.calculate_supoistioner_rating(request)
		super(CustomNutriotionAdmin, self).save_model(request, obj, form, change)


class CustomCompanyAdmin(admin.ModelAdmin):
	model = Company

	fieldsets = (
	(None, {
		'fields': ('name', 'site', 'logo_thumb', 'logo', 'description', 'nation',)
		}),
		('Wygenerowane parametry', {
			'classes': ('collapse',),
			'fields': ('slug', ),
		}),
	)
	readonly_fields = ('logo_thumb', 'slug')
	list_display = ('name', 'nation', 'created', 'updated')


admin.site.register(Nutrition)
admin.site.register(Company, CustomCompanyAdmin)

admin.site.register(Whey, CustomNutriotionAdmin)
admin.site.register(Gainer, CustomNutriotionAdmin)
admin.site.register(Bcaa, CustomNutriotionAdmin)
admin.site.register(Creatine, CustomNutriotionAdmin)
admin.site.register(PreWorkout, CustomNutriotionAdmin)
admin.site.register(Burner, CustomNutriotionAdmin)

admin.site.register(NutritionHunter)

admin.site.register(Flavour)
admin.site.register(NutritionForm)
admin.site.register(Composition)
admin.site.register(CompositionField)
admin.site.register(NutritionComposition)

# class CustomNutritionReviewClassAdmin(admin.TabularInline):
# 	model = NutritionReviewClass

# class ReviewClassAdmin(admin.ModelAdmin):
# 	inlines = [CustomNutritionReviewClassAdmin,]

# class CustomNutritionReviewAdmin(admin.ModelAdmin):
# 	inlines = [CustomNutritionReviewClassAdmin, ]

# admin.site.register(ReviewClass,ReviewClassAdmin)
# admin.site.register(ReviewField)
# admin.site.register(NutritionReviewClass)
# admin.site.register(NutritionReview, CustomNutritionReviewAdmin)

class CustomReviewFielddAdmin(admin.TabularInline):
	model = ReviewField
	extra = 0
	
class CustomNutritionReviewAdmin(admin.ModelAdmin):
	inlines = [CustomReviewFielddAdmin, ]

admin.site.register(ReviewField)
admin.site.register(NutritionReview, CustomNutritionReviewAdmin)

# SINGLETON CONFIG
class ConfigurationForm(forms.ModelForm):
	class Meta:
		model = Configuration
		widgets = {
			'whey_description': admin.widgets.AdminTextareaWidget,
			'gainer_description': admin.widgets.AdminTextareaWidget,
			'bcaa_description': admin.widgets.AdminTextareaWidget,
			'carbs_description': admin.widgets.AdminTextareaWidget,
			'pre_workout_description': admin.widgets.AdminTextareaWidget,
			'creatine_description': admin.widgets.AdminTextareaWidget,
			'burner_description': admin.widgets.AdminTextareaWidget,
		}
		fields = ()


class CustomConfigurationAdmin(SingletonModelAdmin):
	fieldsets = (
	(None, {
		'fields': ('id_ceneo', )
		}),
		('Opisy katerogii supli', {
			'fields': ('whey_description', 'gainer_description', 'bcaa_description', 'carbs_description', 'pre_workout_description',\
			 'creatine_description', 'burner_description' ),
		}),
		('supPositoner', {
			'classes': ('collapse',),
			'fields': ('supositioner_min', 'supositioner_max', 'supositioner_update'),
		}),
	)
	readonly_fields = ('supositioner_min', 'supositioner_max','supositioner_update')


admin.site.register(Configuration ,CustomConfigurationAdmin)