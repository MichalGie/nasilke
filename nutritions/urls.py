from django.conf.urls import url

from .views import CompanyListView, NutritionListView, NutritionCompanyListView, WheyDetailView, GainerDetailView,\
	 BcaaDetailView, CreatineDetailView, Eror404, Temp, WheyListView, GainerListView, BcaaListView, CreatineListView, PreWorkoutDetailView,\
	 BurnerDetailView

urlpatterns = [
	url(r'^odzywki/$', NutritionListView.as_view(), name='nutritions'),
	url(r'^bialka/$', WheyListView.as_view(), name='whey-list'),
	url(r'^gainery/$', GainerListView.as_view(), name='gainer-list'),
	url(r'^bcaa/$', BcaaListView.as_view(), name='bcaa-list'),
	url(r'^kreatyna/$', CreatineListView.as_view(), name='creatine-list'),

	url(r'^firmy/$', CompanyListView.as_view(), name='companies'),
	url(r'^odzywki-firmy-(?P<company_slug>[\w-]+)/$', NutritionCompanyListView.as_view(), name='company-nutritions'),
	
	url(r'^bialko-(?P<nutrition_slug>[\w-]+)/$', WheyDetailView.as_view(), name='whey'),
	url(r'^gainer-(?P<nutrition_slug>[\w-]+)/$', GainerDetailView.as_view(), name='gainer'),
	url(r'^bcaa-(?P<nutrition_slug>[\w-]+)/$', BcaaDetailView.as_view(), name='bcaa'),
	url(r'^spalacz-(?P<nutrition_slug>[\w-]+)/$', BurnerDetailView.as_view(), name='burner'),
	url(r'^przedtreningowka-(?P<nutrition_slug>[\w-]+)/$', PreWorkoutDetailView.as_view(), name='pre-workout'),
	url(r'^kreatyna-(?P<nutrition_slug>[\w-]+)/$', CreatineDetailView.as_view(), name='creatine'),
	url(r'^404/$', Eror404.as_view(), name='error404'),
	url(r'^temp/$', Temp.as_view(), name='temp'),
]