from django.contrib import messages

class FlashMessage():

	ADDED_NEW_REVIEW 	= 'Dodano recenzję, wkrótce pojawi się ona publicznie'
	CONFIRM_EMAIL 		= 'Wysłano email na podany adres {}. Kliknij w link potwierdzający.'

	def new_review(self, request, author):
		if author:
			messages.add_message(request, messages.INFO, self.ADDED_NEW_REVIEW)

			if not author.confirmed:
				messages.add_message(request, messages.INFO, self.CONFIRM_EMAIL.format(author.email))