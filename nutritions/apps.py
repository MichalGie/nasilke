from django.apps import AppConfig


class NutritionConfig(AppConfig):
    name = 'nutritions'
