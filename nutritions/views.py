from django.core.urlresolvers import resolve
from django.shortcuts import redirect, render
from django.views import View
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import UpdateView

# Create your views here.
from .forms import WheyReviewForm, PreWorkoutReviewForm
from .messages import FlashMessage
from .models import Company, Whey, Gainer, Bcaa, Creatine, PreWorkout, Nutrition, Burner, NutritionReview, ReviewField
from users.models import AnonymUser

class Temp(View):
	def get(self, request, *args, **kwargs):
		return render(request, 'home.html', {})

class Eror404(View):
	def get(self, request, *args, **kwargs):
		return render(request, '404.html', {})

class CompanyListView(ListView):
	model = Company

class NutritionListView(ListView):
	model = Nutrition
	paginate_by = 6
	template_name = "nutritions/nutrition_list.html"

	def get_queryset(self):
		return Nutrition.objects.get_queryset().order_by('-id')

	def get_context_data(self, **kwargs):
		context = super(NutritionListView, self).get_context_data(**kwargs)
		context['heading'] = 'Wszystkie suplementy'
		context['url_name'] = resolve(self.request.path_info).url_name
		return context

class WheyListView(NutritionListView):
	model = Whey

	def get_queryset(self):
		return Whey.objects.get_queryset().order_by('-id')


class GainerListView(NutritionListView):
	model = Gainer

	def get_queryset(self):
		return Gainer.objects.get_queryset().order_by('-id')

class BcaaListView(NutritionListView):
	model = Bcaa

	def get_queryset(self):
		return Bcaa.objects.get_queryset().order_by('-rating_supositioner')

class CreatineListView(NutritionListView):
	model = Creatine

	def get_queryset(self):
		return Creatine.objects.get_queryset().order_by('-id')



class NutritionCompanyListView(ListView):
	model = Nutrition
	company = None
	template_name = "nutritions/nutrition_company_list.html"

	def get(self, request, *args, **kwargs):
		self.company = self.kwargs['company_slug']
		return super(NutritionCompanyListView, self).get(request, *args, **kwargs)

	def get_queryset(self):
		if self.company:
			return Nutrition.objects.filter(company__slug=self.company)

	def get_context_data(self, **kwargs):
		# Call the base implementation first to get a context
		context = super(NutritionCompanyListView, self).get_context_data(**kwargs)
		company = Company.objects.get(slug=self.company)
		context['company_logo'] = company.logo.url
		context['company_name'] = company.name
		context['company_site'] = company.site
		context['company_description'] = company.description

		return context


class WheyDetailView(View):

	def get(self, request, *args, **kwargs):
		obj = Whey.objects.get(slug=self.kwargs.get("nutrition_slug"))
		
		form = WheyReviewForm(request.POST or None)
		rank_form = [form['solubility'], form['taste']]

		reviews = NutritionReview.cobjects.filter(nutrition=obj)

		context = {
			'object'	: obj,
			'form' 		: form,
			'rank_form' : rank_form,
			'reviews' 	: reviews,
			'rank'		: 3,
		}

		return render(request, 'nutritions/nutrition_detail.html', context)

	def post(self, request, *args, **kwargs):
		obj = Whey.objects.get(slug=self.kwargs.get("nutrition_slug"))
		form = WheyReviewForm(request.POST or None)
		author = form.review_save(obj)
		FlashMessage().new_review(request, author)

		return redirect('whey', nutrition_slug=self.kwargs.get("nutrition_slug"))


class BurnerDetailView(View):
	def get(self, request, *args, **kwargs):
		obj = Burner.objects.get(slug=self.kwargs.get("nutrition_slug"))
		
		form = BurnerReviewForm(request.POST or None)
		rank_form = [form['solubility'], form['taste']]

		reviews = NutritionReview.cobjects.filter(nutrition=obj)

		context = {
			'object'	: obj,
			'form' 		: form,
			'rank_form' : rank_form,
			'reviews' 	: reviews,
		}

		return render(request, 'nutritions/nutrition_detail.html', context)

	def post(self, request, *args, **kwargs):
		obj = Burner.objects.get(slug=self.kwargs.get("nutrition_slug"))
		form = BurnerReviewForm(request.POST or None)
		author = form.review_save(obj)
		FlashMessage().new_review(request, author)

		return redirect('burner', nutrition_slug=self.kwargs.get("nutrition_slug"))

class PreWorkoutDetailView(View):

	def get(self, request, *args, **kwargs):
		obj = PreWorkout.objects.get(slug=self.kwargs.get("nutrition_slug"))
		
		form = PreWorkoutReviewForm(request.POST or None)

		rank_form = []
		for field in form.get_review_fields():
			rank_form.append(form[field])

		reviews = NutritionReview.cobjects.filter(nutrition=obj)

		context = {
			'object'	: obj,
			'form' 		: form,
			'rank_form' : rank_form,
			'reviews' 	: reviews,
			'rank' 		: 3,
		}

		return render(request, 'nutritions/nutrition_detail.html', context)

	def post(self, request, *args, **kwargs):
		obj = PreWorkout.objects.get(slug=self.kwargs.get("nutrition_slug"))
		form = PreWorkoutReviewForm(request.POST or None)
		author = form.review_save(obj)
		FlashMessage().new_review(request, author)

		return redirect('pre-workout', nutrition_slug=self.kwargs.get("nutrition_slug"))

class GainerDetailView(DetailView):
	model = Gainer
	template_name = "nutritions/nutrition_detail.html"

	def get_object(self):
		return Gainer.objects.get(slug=self.kwargs.get("nutrition_slug"))

	def get_context_data(self, **kwargs):
		# Call the base implementation first to get a context
		context = super(GainerDetailView, self).get_context_data(**kwargs)
		print(context)
		return context

class BcaaDetailView(DetailView):
	model = Bcaa
	template_name = "nutritions/nutrition_detail.html"
	

	def get_context_data(self, **kwargs):
		obj = Bcaa.objects.get(slug=self.kwargs.get("nutrition_slug"))
		compositions = list()
		for comp in obj.composition.all():
			comp = comp.nutrition_composition
			compositions.append(comp)

		context = super(BcaaDetailView, self).get_context_data(**kwargs)
		context['compositions'] = compositions
		context['rank'] = round(obj.rating_supositioner *2 ) / 2
		return context

	def get_object(self):
		return  Bcaa.objects.get(slug=self.kwargs.get("nutrition_slug"))

class CreatineDetailView(DetailView):
	model = Creatine
	template_name = "nutritions/nutrition_detail.html"
	
	def get_object(self):
		return Creatine.objects.get(slug=self.kwargs.get("nutrition_slug"))