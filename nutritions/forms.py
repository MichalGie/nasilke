from django import forms
from django.core.validators import MaxValueValidator, MinValueValidator, MinLengthValidator
from django.contrib import messages

from users.models import AnonymUser
from .models import NutritionReview, ReviewField

class NutritionBaseReviewForm(forms.Form):
	nick			= forms.CharField(max_length=32, validators=[MinLengthValidator(2)])
	email			= forms.EmailField(required=True)
	review 			= forms.CharField(max_length=255, widget=forms.Textarea, required=True)

	def save_base(self):
		if self.is_valid():
			nick 	= self.cleaned_data['nick']
			email 	= self.cleaned_data['email']
			review  = self.cleaned_data['review']

			author = AnonymUser.create_anonym_user(email, nick)

			return author, review
		print('valid error')

class WheyReviewForm(NutritionBaseReviewForm):
	solubility		= forms.IntegerField(label='Rozpuszczalność' , required=True, validators=[MinValueValidator(1), MaxValueValidator(5)], widget=forms.HiddenInput())
	taste			= forms.IntegerField(label='Smak', required=True, validators=[MinValueValidator(1), MaxValueValidator(5)], widget=forms.HiddenInput())

	def get_review_fields(self):
		return ['solubility', 'taste', ]

	def review_save(self, nutrition):
		if self.is_valid():
			solubility 	= self.cleaned_data['solubility']
			taste		= self.cleaned_data['taste']
		
			author, review = self.save_base()

			nutirition_review = NutritionReview(nutrition=nutrition, author=author, review=review)
			nutirition_review.save()

			rw1 = ReviewField(field='RW1', value=int(solubility), review=nutirition_review)
			rw1.save()

			rw2 = ReviewField(field='RW2', value=int(taste), review=nutirition_review)
			rw2.save()

			return author


class PreWorkoutReviewForm(NutritionBaseReviewForm):
	pump			= forms.IntegerField(required=True, validators=[MinValueValidator(1), MaxValueValidator(5)], widget=forms.HiddenInput())
	concentration	= forms.IntegerField(required=True, validators=[MinValueValidator(1), MaxValueValidator(5)], widget=forms.HiddenInput())
	stimulation		= forms.IntegerField(required=True, validators=[MinValueValidator(1), MaxValueValidator(5)], widget=forms.HiddenInput())
	worktime 		= forms.IntegerField(required=True, validators=[MinValueValidator(1), MaxValueValidator(5)], widget=forms.HiddenInput())

	pump.label			= 'Pompa'
	concentration.label = 'Koncentracja'
	stimulation.label 	= 'Pobudzenie'
	worktime.lebel 		= 'Czas działania'

	def get_review_fields(self):
		return ['pump', 'concentration', 'stimulation', 'worktime', ]

	def review_save(self, nutrition):
		if self.is_valid():
			pump 			= self.cleaned_data['pump']
			concentration	= self.cleaned_data['concentration']
			stimulation		= self.cleaned_data['stimulation']
			worktime		= self.cleaned_data['worktime']
		
			author, review = self.save_base()

			nutirition_review = NutritionReview(nutrition=nutrition, author=author, review=review)
			nutirition_review.save()

			rp1 = ReviewField(field='RP1', value=int(pump), review=nutirition_review)
			rp1.save()

			rp2 = ReviewField(field='RP2', value=int(concentration), review=nutirition_review)
			rp2.save()

			rp3 = ReviewField(field='RP3', value=int(stimulation), review=nutirition_review)
			rp3.save()

			rp4 = ReviewField(field='RP4', value=int(worktime), review=nutirition_review)
			rp4.save()

			return author

class BurnerReviewForm(NutritionBaseReviewForm):
	stimulation			= forms.IntegerField(label='Pobudzenie' , required=True, validators=[MinValueValidator(1), MaxValueValidator(5)], widget=forms.HiddenInput())
	phisical_comfort	= forms.IntegerField(label='Komfort psychiczny', required=True, validators=[MinValueValidator(1), MaxValueValidator(5)], widget=forms.HiddenInput())
	no_appetite			= forms.IntegerField(label='Brak apetytu', required=True, validators=[MinValueValidator(1), MaxValueValidator(5)], widget=forms.HiddenInput())

	def get_review_fields(self):
		return ['stimulation', 'phisical_comfort', 'no_appetite', ]

	def review_save(self, nutrition):
		if self.is_valid():

			stimulation 		= self.cleaned_data['stimulation']
			phisical_comfort 	= self.cleaned_data['phisical_comfort']
			no_appetite 		= self.cleaned_data['no_appetite']
		
			author, review = self.save_base()

			nutirition_review = NutritionReview(nutrition=nutrition, author=author, review=review)
			nutirition_review.save()

			rw1 = ReviewField(field='RB1', value=int(stimulation), review=nutirition_review)
			rw1.save()

			rw2 = ReviewField(field='RB2', value=int(phisical_comfort), review=nutirition_review)
			rw2.save()

			rw3 = ReviewField(field='RB2', value=int(no_appetite), review=nutirition_review)
			rw3.save()

			return author