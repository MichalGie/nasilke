from ckeditor_uploader.fields import RichTextUploadingField

from django.contrib import messages
from django.contrib.auth.models import User
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.db.models import Q
from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver
from django.utils.safestring import mark_safe
from solo.models import SingletonModel
# Create your models here.

from .utils import unique_slug_generator
from users.models import AnonymUser

#comapnies choices
POL  	= 'Polska'
USA 	= 'USA'
REST 	= 'Reszta Świata'

NATION_COMPANY_CHOICES = (
	('P', POL),
	('U', USA),
	('R', REST),
	)

#review choices
# Whey
RW1 = 'Rozpuszczalność'
RW2 = 'Smak'

# Pre-workout
RP1 = 'Pompa'
RP2 = 'Koncentracja'
RP3 = 'Pobudzenie'
RP4 = 'Czas działania'

# Burner
RB1 = 'Pobudzenie'
RB2 = 'Komfort psychiczny'
RB3 = 'Brak apetytu'

# GAINER


REVIEW_NUTRITION_FIELDS_CHOICES = (
		('RW1',RW1),
		('RW2',RW2),
		('RP1',RP1),
		('RP2',RP2),
		('RP3',RP3),
		('RP4',RP4),
		('RB1',RB1),
		('RB2',RB2),
		('RB3',RB3),
	)


class Company(models.Model):
	name 			= models.CharField(max_length=255, blank=False, null=False, unique=True, verbose_name='Nazwa firmy')
	site 			= models.CharField(max_length=255, blank=False, null=False, verbose_name='Adres strony www')
	description		= models.TextField(blank=False, null=False, verbose_name='Opis')
	logo 			= models.ImageField(upload_to='img/company/', blank=False, null=False, verbose_name='Logo')
	nation			= models.CharField(max_length=1, choices=NATION_COMPANY_CHOICES, default=POL, blank=False, null=False, verbose_name='Kraj')
	slug  			= models.SlugField(blank=True, verbose_name='Wygenerowany slug z nazwy')
	updated			= models.DateTimeField(auto_now=True)
	created		 	= models.DateTimeField(auto_now_add=True)

	class Meta:
		verbose_name = 'Firma'
		verbose_name_plural = '10. Firmy'

	def __str__(self):
		return self.name

	def save(self, *args, **kwargs):
		self.slug = unique_slug_generator(self)
		super(Company, self).save(*args, **kwargs)

	def logo_thumb(self):
		if self.logo:
			return mark_safe('<img src="{}" style="width:200px" />'.format(self.logo.url))
		else:
			return "Brak wgranego loga"

#NUTRITIONS
class Flavour(models.Model):
	name 			= models.CharField(max_length=255, blank=False, null=False, unique=True, verbose_name='Smak')

	def __str__(self):
		return self.name

	class Meta:
		verbose_name = 'Smak suplementu'
		verbose_name_plural = '20. Smaki suplementów'


class NutritionForm(models.Model):
	name 			= models.CharField(max_length=255, blank=False, null=False, unique=True, verbose_name='Forma supla')

	def __str__(self):
		return self.name

	class Meta:
		verbose_name = 'Forma suplementu'
		verbose_name_plural = '22. Formy suplementów'

class Nutrition(models.Model):
	name 				= models.CharField(max_length=255, blank=False, null=False, verbose_name='Nazwa')
	ceneo_id			= models.PositiveIntegerField(blank=False, null=False, unique=False, verbose_name='ID ceneo')
	company 			= models.ForeignKey(Company, verbose_name='Producent')
	photo				= models.ImageField(upload_to='img/nutrition/', blank=True, null=True, verbose_name='Zdjęcie')
	price 				= models.FloatField(blank=False, null=False, verbose_name='Cena (zł)')
	weight 				= models.PositiveSmallIntegerField(blank=False, null=False, verbose_name='Waga (g)')
	portion 			= models.PositiveSmallIntegerField(blank=False, null=False, verbose_name='Porcja (g)')
	kcal 				= models.PositiveSmallIntegerField(blank=False, null=False, verbose_name='Wartość kaloryczna w 100g (kcal)')
	form 				= models.ForeignKey(NutritionForm, blank=True, null=True, verbose_name='Forma supla')
	flavour 			= models.ManyToManyField(Flavour, verbose_name='Smak')
	description 		= RichTextUploadingField(blank=False, null=False, verbose_name='Opis')
	expert_note			= models.CharField(max_length=255, blank=True, null=True, verbose_name='Opinia experta')
	featured			= models.BooleanField(default=False, verbose_name='Rekomendacja')
	available			= models.BooleanField(default=True, verbose_name='Dostępne na rynku')
	rating				= models.FloatField(blank=True, null=True, verbose_name='Wyliczona wartość')
	rating_supositioner	= models.FloatField(blank=True, null=True, verbose_name='Wyliczona ocena suPositioner')
	portion_amount		= models.PositiveSmallIntegerField(blank=True, null=False, verbose_name='Wyl ilość porcji w opak.')
	slug  				= models.SlugField(blank=True, verbose_name='Wygenerowany slug z nazwy')
	updated				= models.DateTimeField(auto_now=True)
	created		 		= models.DateTimeField(auto_now_add=True)

	class Meta:
		verbose_name = 'Wszystkie suplementy'
		verbose_name_plural = '07. Wszystkie suplementy'

	def photo_thumb(self):
		if self.photo:
			return mark_safe('<img src="{}" style="width:250px;"/>'.format(self.photo.url))
		else:
			return 'Brak wgranego zdjęcia'
	photo_thumb.short_description = 'Zdjęcie'
	

	def get_all_compositions(self):
		return self.composition.all()

	def get_composition(self, name, request):
		obj = self.get_all_compositions().filter(nutrition_composition__field__name=name)

		if not obj:
			messages.warning(request, 'Brak pola {}'.format(name))
			return None

		return obj[0].nutrition_composition.value

	def save(self, *args, **kwargs):
		self.slug = unique_slug_generator(self)
		if self.portion and self.weight:
			self.portion_amount = self.weight / self.portion
		super(Nutrition, self).save(*args, **kwargs)


	def __str__(self):
		return self.name


class CompositionField(models.Model):
	name 			= models.CharField(max_length=255, blank=False, null=False, unique=True, verbose_name='Nazwa składnika')

	def __str__(self):
		return self.name

	class Meta:
		verbose_name = 'Składnik'
		verbose_name_plural = '21. Składniki'

class NutritionComposition(models.Model):
	field 					= models.ForeignKey(CompositionField, blank=False, null=True, verbose_name='Nazwa składnika')
	value 					= models.FloatField(blank=False, null=False, validators=[MinValueValidator(1), MaxValueValidator(100)], verbose_name='Wartość na 100g (g)')

	def __str__(self):
		return str(self.field) + ': ' + str(self.value) + 'g'

	class Meta:
		verbose_name = 'Składnik suplemnetu'
		verbose_name_plural = '22. Składniki suplementów'


class Composition(models.Model):
	nutrition 				= models.ForeignKey(Nutrition, blank=False, null=True, related_name='composition', verbose_name='Suplement')
	nutrition_composition 	= models.ForeignKey(NutritionComposition, blank=False, null=True, verbose_name='Skład')

	def __str__(self):
		return str(self.nutrition) + " : " +  str(self.nutrition_composition)

	class Meta:
		verbose_name = 'Skład suplementu'
		verbose_name_plural = '24. Składy suplementów'


class Whey(Nutrition):

	class Meta:
		verbose_name = 'Białko'
		verbose_name_plural = '01. Białka'

	def __str__(self):
		return self.nutrition_ptr.name

	def calculate_supoistioner_rating(self, request):
		return 10


class Gainer(Nutrition):

	class Meta:
		verbose_name = 'Gainer'
		verbose_name_plural = '02. Gainery'

	def __str__(self):
		return self.nutrition_ptr.name


class Bcaa(Nutrition):

	class Meta:
		verbose_name = 'Bcaa'
		verbose_name_plural = '03. Bcaa'

	def calculate_supoistioner_rating(self, request):
		lleucine 	= self.get_composition('L-leucine', request)
		lisoleucine = self.get_composition('L-isoleucine', request)
		lvaline 	= self.get_composition('L-valine', request)

		if None in (lleucine, lisoleucine, lvaline):
			pass
		else:
			nutrition = self.nutrition_ptr
			amino = lleucine + lisoleucine + lvaline
			amino_in_portion = (((nutrition.portion*(amino))/100))
			price_per_portion = (nutrition.weight/nutrition.portion)/nutrition.price
			lleucine_in_portion = (nutrition.portion*lleucine/100)
			company = 7
			return((nutrition.portion_amount*2)+(amino_in_portion*6)+(price_per_portion*4)+(lleucine_in_portion*8)+(company*1))/(2+6+4+8+1)
			# return ((nutrition.price * 100 / nutrition.weight) / ( (amino / nutrition.weight)*0.4 + (lleucine / amino)* 0.6 ))

	def __str__(self):
		return self.nutrition_ptr.name


M = "Monohydrat kreatyny"
J = "Jabłczan kreatyny"
CREATINE_FORM_CHOICES = (
		('M', M),
		('J', J),
	)

class Creatine(Nutrition):
	class Meta:
		verbose_name = 'Kreatyna'
		verbose_name_plural = '04. Kreatyny'

	creatine_form  	= models.CharField(max_length=1, choices=CREATINE_FORM_CHOICES, default=M, blank=True, null=False, verbose_name='Forma kreatyny')

	def __str__(self):
		return self.nutrition_ptr.name



class PreWorkout(Nutrition):

	class Meta:
		verbose_name = 'PreWorkout'
		verbose_name_plural = '05. PreWorkout'

	def __str__(self):
		return self.nutrition_ptr.name

	def calculate_supoistioner_rating(self, request):
		return 10

class Burner(Nutrition):

	class Meta:
		verbose_name = 'Burner'
		verbose_name_plural = '06. Burner'

	def __str__(self):
		return self.nutrition_ptr.name

	def calculate_supoistioner_rating(self, request):
		return 10
# REVIEWS

class CustomNutritionReviewManager(models.Manager):
	def get_queryset(self):
		return super(CustomNutritionReviewManager, self).get_queryset().filter(Q(approved=True) & Q(author__confirmed=True) & Q(author__banned=False))


class NutritionReview(models.Model):
	# nutrition_type 			= models.CharField(max_length=1, choices=REVIEW_NUTRITION_CHOICES, default=R1, blank=False, null=False)
	nutrition 				= models.ForeignKey(Nutrition, blank=False, null=True)
	author	 				= models.ForeignKey(AnonymUser, blank=False, null=True)
	review 					= models.CharField(max_length=255, blank=False, null=False)
	
	approved 				= models.BooleanField(default=False)
	created 				= models.DateTimeField(auto_now_add=True)

	objects 				= models.Manager()
	cobjects 				= CustomNutritionReviewManager()


	class Meta:
		verbose_name = 'Recenzja'
		verbose_name_plural = 'Recenzje'

	def __str__(self):
		return self.nutrition.name + ' (' + self.author.email + ')'


class ReviewField(models.Model):
	field 					= models.CharField(max_length=3, choices=REVIEW_NUTRITION_FIELDS_CHOICES, default=RW1, blank=False, null=False)
	value 					= models.PositiveSmallIntegerField(validators=[MinValueValidator(1), MaxValueValidator(5)], blank=False, null=False)
	review					= models.ForeignKey(NutritionReview, related_name='review_fields', blank=False, null=False)

	def __str__(self):
		return self.review.nutrition.name + ' (' + self.get_field_display() + ': ' + str(self.value) +')'
	

	class Meta:
		verbose_name = 'Pole recenzji'
		verbose_name_plural = 'Pola recenzji'


class Configuration(SingletonModel):
	id_ceneo				= models.CharField(max_length=10, blank=False, null=False, verbose_name='id ceneo')

	whey_description		= models.CharField(max_length=2555, blank=False, null=False, verbose_name='opis białka')
	gainer_description		= models.CharField(max_length=2555, blank=False, null=False, verbose_name='opis gainera')
	bcaa_description		= models.CharField(max_length=2555, blank=False, null=False, verbose_name='opis bcaa')
	carbs_description		= models.CharField(max_length=2555, blank=False, null=False, verbose_name='opis węglowodanów')
	pre_workout_description	= models.CharField(max_length=2555, blank=False, null=False, verbose_name='opis przedtreningówki')
	creatine_description	= models.CharField(max_length=2555, blank=False, null=False, verbose_name='opis kreatyny')
	burner_description		= models.CharField(max_length=2555, blank=False, null=False, verbose_name='opis spalacza')

	supositioner_min		= models.FloatField(blank=True, null=True, verbose_name='Wyliczony min. współczynnik supositionera Bcaa')
	supositioner_max		= models.FloatField(blank=True, null=True, verbose_name='Wyliczony max. współczynnik supositionera Bcaa')
	supositioner_update		= models.DateTimeField(blank=True, null=True, verbose_name='Update supositionera')

	class Meta:
		verbose_name = '99. Ustawienia'

	def save(self, *args, **kwargs):
		bcaa = Bcaa.objects.all()
		bcaa_min = Bcaa.objects.order_by('nutrition_ptr__rating')[:1].get()
		bcaa_max = Bcaa.objects.order_by('-nutrition_ptr__rating')[:1].get()
		min_value = 4.6
		max_value = 2.6

		print(bcaa_min)
		print(bcaa_max)
		
		for obj in bcaa:
			obj.rating_supositioner = min_value + (( max_value - min_value ) / (bcaa_max.nutrition_ptr.rating - bcaa_min.nutrition_ptr.rating))*(obj.rating - bcaa_min.nutrition_ptr.rating)
			obj.save()




class NutritionHunter(models.Model):
	nutrition 	= models.ForeignKey(Nutrition, blank=False, null=False)
	price 		= models.FloatField(blank=False, null=False)
	discount 	= models.FloatField(blank=True, null=False)
	link 		= models.CharField(max_length=256, blank=False, null=False)
	available 	= models.BooleanField(default=True, blank=False, null=False)
	updated 	= models.DateTimeField(auto_now=True)
	created 	= models.DateTimeField(auto_now_add=True)

	def __str__(self):
		return self.nutrition.name + ' (' + str(self.discount) + '%)'

	def save(self, *args, **kwargs):
		self.discount = round(100*self.price/self.nutrition.price)
		super(NutritionHunter, self).save(*args, **kwargs)

	class Meta:
		verbose_name = 'supleHunter'
		verbose_name_plural = 'supleHunter'


@receiver(pre_save, sender=CompositionField, dispatch_uid='composition_field_pre_save_signal')
def composition_field_pre_save_signal(sender, instance, **kwargs):
	instance.name = instance.name.capitalize()

@receiver(pre_save, sender=Flavour, dispatch_uid='flavour_pre_save_signal')
def flavour_pre_save_signal(sender, instance, **kwargs):
	instance.name = instance.name.capitalize()

@receiver(pre_save, sender=Company, dispatch_uid='company_pre_save_signal')
def composition_field_capitalize_signal(sender, instance, **kwargs):
	instance.name = instance.name.capitalize()
	
	if 'http://' not in instance.site:
		instance.site = 'http://' + instance.site
