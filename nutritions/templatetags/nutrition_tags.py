from django import template
from django.urls import reverse
from django.http import HttpResponseRedirect

register = template.Library()

@register.simple_tag
def get_url(object):
	try:
		whey = object.whey
		return HttpResponseRedirect(reverse('whey', kwargs={'nutrition_slug': object.slug})).url
	except:
		pass

	try:
		gainer = object.gainer
		return HttpResponseRedirect(reverse('gainer', kwargs={'nutrition_slug': object.slug})).url
	except:
		pass

	try:
		bcaa = object.bcaa
		return HttpResponseRedirect(reverse('bcaa', kwargs={'nutrition_slug': object.slug})).url
	except:
		pass

	try:
		creatine = object.creatine
		return HttpResponseRedirect(reverse('creatine', kwargs={'nutrition_slug': object.slug})).url
	except:
		pass

	try:
		preworkout = object.preworkout
		return HttpResponseRedirect(reverse('pre-workout', kwargs={'nutrition_slug': object.slug})).url
	except:
		pass


@register.simple_tag
def get_class(object):
	try:
		whey = object.whey
		return 'whey'
	except:
		pass

	try:
		gainer = object.gainer
		return 'gainer'
	except:
		pass

	try:
		bcaa = object.bcaa
		return 'bcaa'
	except:
		pass

	try:
		creatine = object.creatine
		return 'creatine'
	except:
		pass

	try:
		preworkout = object.preworkout
		return 'preworkout'
	except:
		pass