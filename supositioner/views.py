from django.shortcuts import render
from django.views import View
# Create your views here.


class SupositonerFirst(View):
	def get(self, request, *args, **kwargs):
		return render(request, 'supositioner/supositioner_1.html', {})

class SupositonerSecond(View):
	def get(self, request, *args, **kwargs):
		return render(request, 'supositioner/supositioner_2.html', {})

class SupositonerThird(View):
	def get(self, request, *args, **kwargs):
		return render(request, 'supositioner/supositioner_3.html', {})