from django.conf.urls import url

from .views import SupositonerFirst, SupositonerSecond, SupositonerThird

urlpatterns = [
	url(r'^supositioner/$', SupositonerFirst.as_view(), name='supositioner'),
	url(r'^supositioner/2/$', SupositonerSecond.as_view(), name='supositioner'),
	url(r'^supositioner/3/$', SupositonerThird.as_view(), name='supositioner'),
]